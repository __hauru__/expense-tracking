'use strict';

const _ = require('lodash');
const gulp = require('gulp');
const mergeStreams = require('merge-stream');
const del = require('del');
const Promise = require('bluebird');
const path = require('path');

const ext = {
  sass: require('gulp-ruby-sass'),
  cache: require('gulp-cached'),
  remember: require('gulp-remember'),
  uglify: require('gulp-uglify'),
  filter: require('gulp-filter'),
  debug: require('gulp-debug'),
  nop: require('gulp-nop'),
  include: require('gulp-file-include'),
  if: require('gulp-if'),
  //babel: require('gulp-babel')
};

let config = require('./gulpconf');
let forProduction = false;



function promisify() {
  let args = Array.prototype.slice.call(arguments);
  let stream = args.length < 2 ? args[0] : mergeStreams.apply(null, args);
  
  return new Promise(function(resolve, reject) {
    stream.on('end', function() {
      resolve(stream);
    }).on('error', function() {
      reject();
    });
  });
}

function processScss(dirName) {
  let dir = config.dirs[dirName];
  let baseDir = path.join(dirName, dir.source, 'css');
  let baseOutputDir = path.join(dirName, dir.output, 'css');

  let sassOpts = {
    style: forProduction ? 'compressed' : 'expanded',
    cacheLocation: '.cache/sass'
  };

  let scss = ext.sass(path.join(baseDir, '*.scss'), sassOpts)
    .pipe(gulp.dest(baseOutputDir));

  // also copy plain CSS (with caching)
  let css = gulp.src(path.join(baseDir, '**', '*.css'))
    .pipe(ext.cache('styles'))
    .pipe(gulp.dest(baseOutputDir));

  return promisify(scss, css);
}

function processJs(dirName) {
  let dir = config.dirs[dirName];
  let baseDir = path.join(dirName, dir.source, 'js');
  let jsFiles = [];

  for(let file of dir.jsFiles)  {
    jsFiles.push(path.join(baseDir, file));
  }
  
  function testFileCondition(condition, prod) {
    return function(file) {
      return dir.jsOptions[file.relative][condition] && (typeof(prod) !== 'boolean' || forProduction == prod);
    }
  }
  
  let stream = gulp.src.call(gulp, jsFiles, { base: baseDir })
    .pipe(ext.include({ context: { forProduction } }))
    .pipe(ext.if(testFileCondition('cache'), ext.cache('js')))
    //.pipe(ext.if(testFileCondition('transpile'), ext.babel()))
    .pipe(ext.if(testFileCondition('uglify', true), ext.uglify()))
    .pipe(gulp.dest(path.join(dirName, dir.output, 'js')));
  
  return promisify(stream);
}


function forAllDirs(func) {
  let promises = [];

  for(let key in config.dirs)  {
    promises.push(func(key));
  }

  return Promise.all(promises);
}

function clear() {
  let toClear = [];

  for(let key in config.dirs)  {
    toClear.push(path.join(key, config.dirs[key].output, 'css'));
    toClear.push(path.join(key, config.dirs[key].output, 'js'));
  }

  return del(toClear, { force: true });
}

function processEverything() {
  return Promise.all([
    forAllDirs(processScss),
    forAllDirs(processJs)
  ]);
}

function normalizeConfig(config) {
  let key, dir;
  for(key in config.dirs) {
    dir = config.dirs[key];
    _.defaults(dir, {
      jsFiles: [],
      jsOptions: {}
    });
    
    for(let file of dir.jsFiles) {
      dir.jsOptions[file] = _.assign({
        uglify: true,
        //transpile: false,
        cache: true
      }, dir.jsOptions[file]);
    }
  }
  
  return config;
}


gulp.task('scss', function() {
  return forAllDirs(processScss);
});

gulp.task('js', function() {
  return forAllDirs(processJs);
});

gulp.task('clear', clear);

gulp.task('watch', function() {
  function setUpWatches(dir) {
    let dirOpts = config.dirs[dir];

    gulp.watch(path.join(dir, dirOpts.source, 'css', '**/*'), function() {
      return processScss(dir);
    });

    gulp.watch(path.join(dir, dirOpts.source, 'js', '**/*'), function(event) {
      if(event.type == 'deleted') {
        delete ext.cached.caches['js'][event.path];
        ext.remember.forget('js', event.path);
      }

      return processJs(dir);
    });
  }

  for(let dir in config.dirs)  {
    setUpWatches(dir);
  }
});

gulp.task('all-dev', ['clear'], processEverything);

gulp.task('all-prod', ['clear'], function() {
  forProduction = true;
  return processEverything();
});

normalizeConfig(config);


