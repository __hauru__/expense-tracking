START TRANSACTION;
INSERT INTO `units` (`symbol`, `name`, `symbol_html`) VALUES
  ('kWh', 'kilowatogodzina', 'kWh'),
  ('m3', 'metr sześcienny', 'm<sup>3</sup>'),
  ('J', 'dżul', 'J');
COMMIT;
