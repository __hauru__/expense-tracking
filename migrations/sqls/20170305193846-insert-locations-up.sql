START TRANSACTION;
INSERT INTO `locations` (`name`, `order`) VALUES
  ('Filia nr 2', 1),
  ('Filia nr 3', 2),
  ('Filia nr 4', 3),
  ('Filia nr 5', 4),
  ('Filia nr 6', 5),
  ('Filia nr 7', 6),
  ('Filia nr 8', 7),
  ('Filia nr 9', 8),
  ('Filia nr 10', 9),
  ('Filia nr 11', 10),
  ('Filia nr 12', 11),
  ('Filia nr 13', 12),
  ('Filia nr 14', 13),
  ('Filia nr 15', 14),
  ('Filia nr 16', 15),
  ('Filia nr 17', 16),
  ('Filia nr 18', 17),
  ('Filia nr 19', 18),
  ('Filia nr 20', 19),
  ('Czytelnia', 20),
  ('Dyrekcja', 21);
COMMIT;
