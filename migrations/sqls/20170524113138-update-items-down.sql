START TRANSACTION;

UPDATE items SET `name` = 'Prąd - eksploatacja' WHERE `name` = 'Prąd - dystrybucja';

DELETE e FROM expenses AS e
  LEFT JOIN items AS i ON i.`id` = e.`item_id`
  WHERE i.`name` IN ('Woda', 'Prąd', 'Opłata eksploatacyjna', 'Legalizacja wodomierzy');

DELETE FROM items WHERE `name` IN ('Woda', 'Prąd', 'Opłata eksploatacyjna', 'Legalizacja wodomierzy');

COMMIT;
