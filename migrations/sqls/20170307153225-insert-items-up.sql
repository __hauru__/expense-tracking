START TRANSACTION;
-- [TODO] dodać: OPP, opłata eksploatacyjna
INSERT INTO `items` (`name`, `type`, `flat_rate`, `unit_symbol`) VALUES
  ('Woda zimna', 'water', 0, 'm3'),
  ('Woda ciepła', 'water', 0, 'm3'),
  ('Woda - abonament', 'water', 1, NULL),
  ('Kanalizacja', NULL, 1, NULL),
  ('Ogrzewanie - zamówiona moc', 'heating', 0, 'J'),
  ('Ogrzewanie - CO', 'heating', 0, 'J'),
  ('Czynsz', NULL, 1, NULL),
  ('Gaz', NULL, 0, 'm3'),
  ('Prąd - eksploatacja', 'electricity', 0, 'kWh'),
  ('Prąd - sprzedaż', 'electricity', 0, 'kWh'),
  ('Wywóz odpadów', NULL, 1, NULL),
  ('Ochrona / alarm', NULL, 1, NULL),
  ('Serwis dźwigu', NULL, 1, NULL),
  ('Kominiarz', NULL, 1, NULL);
COMMIT;
