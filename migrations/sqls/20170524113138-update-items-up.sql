START TRANSACTION;

UPDATE items SET `name` = 'Prąd - dystrybucja' WHERE `name` = 'Prąd - eksploatacja';

INSERT INTO `items` (`name`, `type`, `flat_rate`, `unit_symbol`) VALUES
  ('Woda', 'water', 0, 'm3'),
  ('Prąd', 'electricity', 0, 'kWh'),
  ('Opłata eksploatacyjna', NULL, 1, NULL),
  ('Legalizacja wodomierzy', NULL, 1, NULL);

COMMIT;
