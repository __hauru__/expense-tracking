run_cli () {
  
  DIR_="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  
  cd $DIR_/..
  . node_env/bin/activate
  
  set -a
  . .env
  set +a

  node src/cli/$1.js
}
