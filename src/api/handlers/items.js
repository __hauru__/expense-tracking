const db = require('../../db');
const errors = require('../errors');
const { addDefaultErrors } = require('../util');

const Item = db.model('Item');

function getItem(req, res, next) {
  Item.findById(req.params.id)
  .then((result) => {
    if(!result) throw errors.NotFound();
    res.json(result.serialize());
  })
  .catch((err) => { next(err); });
}

getItem.apiDoc = addDefaultErrors({
  summary: 'Fetches an item by ID',
  operationId: 'getItem',
  parameters: [
    { name: 'id', in: 'path', type: 'number', format: 'int32', required: true },
  ],
  responses: {
    200: {
      description: 'The item object',
      schema: { $ref: '#/definitions/Unspecified' }
    }
  }
});

module.exports = [
  { path: '/items/{id}', module: { GET: getItem } }
  
];
