const reportsData = require('../../reports-data');
const { addDefaultErrors } = require('../util');

function expensesByItem(req, res, next) {
  reportsData.expensesByItem(req.query.year, req.query.itemId)
  .then((results) => {
    res.json(results);
  })
  .catch((err) => { next(err); });
}

expensesByItem.apiDoc = addDefaultErrors({
  summary: 'Gets data for a report',
  operationId: 'expensesByItem',
  parameters: [
    { name: 'year', in: 'query', type: 'number', format: 'int32', required: true },
    { name: 'itemId', in: 'query', type: 'number', format: 'int32', required: false }
  ],
  responses: {
    200: {
      description: 'Report data',
      schema: { 
        type: 'array',
        items: { type: 'array' }
      }
    }
  }
});

function expensesByLocation(req, res, next) {
  reportsData.expensesByLocation(req.query.year, req.query.locationId)
  .then((results) => {
    res.json(results);
  })
  .catch((err) => { next(err); });
}

expensesByLocation.apiDoc = addDefaultErrors({
  summary: 'Gets data for a report',
  operationId: 'expensesByLocation',
  parameters: [
    { name: 'year', in: 'query', type: 'number', format: 'int32', required: true },
    { name: 'locationId', in: 'query', type: 'number', format: 'int32', required: false }
  ],
  responses: {
    200: {
      description: 'Report data',
      schema: { 
        type: 'array',
        items: { type: 'array' }
      }
    }
  }
});

function consumptionByItem(req, res, next) {
  reportsData.consumptionByItem(req.query.year, req.query.itemId)
  .then((results) => {
    res.json(results);
  })
  .catch((err) => { next(err); });
}

consumptionByItem.apiDoc = addDefaultErrors({
  summary: 'Gets data for a report',
  operationId: 'consumptionByItem',
  parameters: [
    { name: 'year', in: 'query', type: 'number', format: 'int32', required: true },
    { name: 'itemId', in: 'query', type: 'number', format: 'int32', required: true }
  ],
  responses: {
    200: {
      description: 'Report data',
      schema: { 
        type: 'array',
        items: { type: 'array' }
      }
    }
  }
});


module.exports = [
  { path: '/reports/expenses-by-item', module: { GET: expensesByItem } },
  { path: '/reports/expenses-by-location', module: { GET: expensesByLocation } },
  { path: '/reports/consumption-by-item', module: { GET: consumptionByItem } },
];
