const config = require('config');
const passport = require('passport');

const errors = require('../errors');

function transformAuthError(err) {
  if(typeof(err) == 'string') {
    if(err.startsWith('no such user')) {
      return new errors.Unauthorized();
    }
  } else if(err instanceof Error) {
    if(err.message.startsWith('InvalidCredentialsError:')) {
      return new errors.Unauthorized();
    }
  }
  
  return err;
}

function logIn(req, res, next) {
  passport.authenticate('local', function(err, user) {
    if(err) return next(transformAuthError(err));
    if(user) {
      req.login(user, function(err) {
        if(err) return next(err);
        res.json({ ok: true });
      });
    } else {
      next(new errors.Unauthorized());
    }
  })(req, res, next);
}

logIn.apiDoc = {
  summary: 'Authenticates the user',
  operationId: 'logIn',
  parameters: [
    { name: 'data', in: 'body', schema: { $ref: '#/definitions/LogInRequest' } }
  ],
  responses: {
    200: {
      description: 'User logged in succesfully',
      schema: { $ref: '#/definitions/OkResponse' }
    },
    
    400: { description: 'Validation failed' },
    401: { description: 'Invalid login or password' },
    default: { description: 'An error occurred' }
  }
};

function logOut(req, res, next) {
  const cb = (err) => {
    if(err) return next(err);
    res.json({ ok: true });
  };
  
  if(req.session) {
    res.clearCookie(config.sessions.cookieName); // [TODO] remove this
    req.session.destroy(cb);
  } else {
    cb();
  }
}

logOut.apiDoc = {
  summary: 'Logs the user out (by destroying the session)',
  operationId: 'logOut',
  responses: {
    200: {
      description: 'Always returns 200',
      schema: { $ref: '#/definitions/OkResponse' }
    }
  }
};

function getUser(req, res, next) {
  let result = null;
  if(req.user) {
    result = {
      id: req.user.id,
      login: req.user.login,
      fullName: req.user.fullName
    };
  } 
  
  res.json(result);
};

getUser.apiDoc = {
  summary: 'Gets some info about the current user',
  operationId: 'getUser',
  responses: {
    200: {
      description: 'Returns the UserInfo object, or `null` if not logged in',
      schema: {
        $ref: '#/definitions/UserInfo'
      }
    }
  }
};

module.exports = [
  { path: '/user/log-in', module: { POST: logIn } },
  { path: '/user/log-out', module: { POST: logOut } },
  { path: '/user', module: { GET: getUser } },
]
