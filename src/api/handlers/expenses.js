const _ = require('lodash');
const Serializer = require('sequelize-to-json');
const db = require('../../db');
const errors = require('../errors');
const { addDefaultErrors } = require('../util');


const Expense = db.model('Expense');

function createExpense(req, res, next) {
  
  let data = _.clone(req.body);
  data.lastEditedById = req.user.id;
  
  Expense.create(data)
  .then((inst) => {
    res.json({ id: inst.id });
  })
  .catch((err) => { next(err); });
}

createExpense.apiDoc = addDefaultErrors({
  summary: 'Registers new expense',
  operationId: 'createExpense',
  parameters: [
    { name: 'data', in: 'body', schema: { $ref: '#/definitions/ExpenseCreateRequest' } }
  ],
  responses: {
    200: {
      description: 'Expense added',
      schema: { $ref: '#/definitions/ObjectIdResponse' }
    }
  }
});

function getExpenses(req, res, next) {
  let { year, month, locationId } = req.query;
  Expense.scope('withItem').findAll({
    where: { $and: [{ year }, { month }, { locationId }] },
    order: [
      [db.col('item.name'), 'ASC']
    ]
  })
  .then((expenses) => {
    res.json(Expense.serializeMany(expenses, 'list'));
  })
  .catch((err) => { next(err); });
}

getExpenses.apiDoc = addDefaultErrors({
  summary: 'Gets the list of expenses for the given year / month / location',
  operationId: 'getExpenses',
  parameters: [
    { name: 'year', in: 'query', type: 'number', format: 'int32', required: true },
    { name: 'month', in: 'query', type: 'number', format: 'int32', minimum: 1, maximum: 12, required: true },
    { name: 'locationId', in: 'query', type: 'number', format: 'int32', required: true }
  ],
  responses: {
    200: {
      description: 'List of expenses',
      schema: { 
        type: 'array',
        items: { $ref: '#/definitions/Unspecified' }
      }
    }
  }
});

function deleteExpense(req, res, next) {
  Expense.destroy({
    where: { id: req.params.id }
  })
  .then((n) => {
    if(!n) throw new errors.NotFound();
    res.json({ ok: true });
  })
  .catch((err) => { next(err); });
}

deleteExpense.apiDoc = addDefaultErrors({
  summary: 'Deletes the expense',
  operationId: 'deleteExpense',
  parameters: [
    { name: 'id', in: 'path', type: 'number', format: 'int32', required: true },
  ],
  responses: {
    200: {
      description: 'Expense has been removed',
      schema: { $ref: '#/definitions/OkResponse' }
    }
  }
});

function getExpense(req, res, next) {
  Expense.findById(req.params.id)
  .then(function(inst) {
    if(!inst) throw new errors.NotFound();
    res.json(inst.serialize('default'));
  })
  .catch((err) => { next(err); });
}

getExpense.apiDoc = addDefaultErrors({
  summary: 'Gets an expense',
  operationId: 'getExpense',
  parameters: [
    { name: 'id', in: 'path', type: 'number', format: 'int32', required: true },
  ],
  responses: {
    200: {
      description: 'Expense object',
      schema: { $ref: '#/definitions/Expense' }
    }
  }
});

function updateExpense(req, res, next) {
  let data = _.clone(req.body);
  let transaction;
  
  db.transaction()
  .then((t) => {
    transaction = t;
    return Expense.findById(req.params.id);
  })
  .then((inst) => {
    if(!inst) throw new errors.NotFound();
    Object.assign(inst, data);
    inst.updateGrossCost();
    return inst.save();
  })
  .then(() => {
    return transaction.commit();
  })
  .then(() => {
    res.json({ ok: true });
  })
  .catch((err) => { next(err); });
}

updateExpense.apiDoc = addDefaultErrors({
  summary: 'Updates the expense',
  operationId: 'updateExpense',
  parameters: [
    { name: 'id', in: 'path', type: 'number', format: 'int32', required: true },
    { name: 'data', in: 'body', schema: { $ref: '#/definitions/ExpenseUpdateRequest' } }
  ],
  responses: {
    200: {
      description: 'Expense updated',
      schema: { $ref: '#/definitions/OkResponse' }
    }
  }
});

module.exports = [
  { path: '/expenses/create', module: { POST: createExpense } },
  { path: '/expenses', module: { GET: getExpenses } },
  { path: '/expenses/{id}/delete', module: { POST: deleteExpense } },
  { path: '/expenses/{id}/update', module: { POST: updateExpense } },
  { path: '/expenses/{id}', module: { GET: getExpense } },
];
