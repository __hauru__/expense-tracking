function test(req, res, next) {
  res.json({ ok: true }).end();
}

test.apiDoc = {
  summary: 'Tests the API',
  operationId: 'test',
  parameters: [
    
  ],
  responses: {
    200: {
      description: 'Works!',
      schema: {
        type: 'object',
        properties: {
          ok: { type: 'boolean' }
        }
      }
    },
    default: {
      description: 'An error occurred',
      schema: {
        additionalProperties: true
      }
    }
  }
};


function calcSum(req, res, next) {
  let data = req.body;
  res.json({
    n1: data.n1,
    n2: data.n2,
    sum: data.n1 + data.n2,
  }).end();
}

calcSum.apiDoc = {
  summary: 'Adds two numbers',
  operationId: 'calcSum',
  parameters: [
    { name: 'data', in: 'body', schema: { $ref: '#/definitions/SumRequest' } }
  ],
  responses: {
    200: {
      description: 'Returns the result',
      schema: {
        additionalProperties: true
      }
    },
    default: {
      description: 'An error occurred',
      schema: {
        additionalProperties: true
      }
    }
  }
};


module.exports = [
  { path: '/test/status', module: { GET: test } },
  { path: '/test/sum', module: { POST: calcSum } },
];
