const _ = require('lodash');

const empty = { $ref: '#/definitions/EmptyResponse' };
const unspecified = { $ref: '#/definitions/Unspecified' };

function addDefaultErrors(apiDoc) {
  if(!apiDoc.responses) apiDoc.responses = {};
  
  _.defaults(apiDoc.responses, {
    400: { description: 'Validation failed', schema: unspecified },
    401: { description: 'Unauthorized', schema: empty },
    404: { description: 'Resource not found', schema: empty },
    default: { description: 'An error occurred', schema: unspecified }
  });
  
  return apiDoc;
}

module.exports = {
  addDefaultErrors
};

