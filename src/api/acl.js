const errors = require('./errors');

const re = (s) => new RegExp(s);

const ACL = [
  ['/user/log-in', ['guest']],
  ['/user/log-out', ['user']],
  [re('^/expenses(?:/.*)?$'), ['user']],
  [re('^/reports(?:/.*)?$'), ['user']],
];

module.exports = function(req, res, next) {
  let role = req.user ? 'user' : 'guest';
  let pass = null;
  let pattern, roles, roleOk;
  
  for([pattern, roles] of ACL) {
    roleOk = roles.includes(role);
    if(typeof(pattern) == 'string') {
      if(req.path == pattern) {
        pass = roleOk;
      }
    } else if(pattern instanceof RegExp) {
      if(pattern.test(req.path)) {
        pass = roleOk;
      }
    }
    
    if(pass !== null) break;
  }
    
  if(pass !== false) {
    next();
  } else {
    next(new errors.Unauthorized());
  }
};
