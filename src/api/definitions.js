
const Unspecified = 

module.exports = {
    
  Unspecified: {
    type: 'object',
    description: 'Unspecified object',
    additionalProperties: true
  },
  
  EmptyResponse: {
    type: 'object',
    description: 'Empty response',
    additionalProperties: false
  },
  
  OkResponse: {
    type: 'object',
    properties: {
      ok: { type: 'boolean', enum: [true] }
    },
    required: ['ok']
  },
  
  ObjectIdResponse: {
    type: 'object',
    properties: {
      id: { type: 'number', format: 'int32', minimum: 1 }
    },
    required: ['id']
  },
  
  UserInfo: {
    type: 'object',
    properties: {
      id: { type: 'number', format: 'int32' },
      login: { type: 'string' },
      fullName: { type: 'string' }
    },
    required: ['id', 'login', 'fullName']
  },
  
  LogInRequest: {
    type: 'object',
    properties: {
      login: { type: 'string' },
      password: { type: 'string' }
    },
    required: ['login', 'password']
  },
  
  //SumRequest: {
    //type: 'object',
    //properties: {
      //n1: { type: 'number' },
      //n2: { type: 'number' },
    //},
    //required: ['n1', 'n2']
  //},
  
  ExpenseCreateRequest: {
    type: 'object',
    properties: {
      itemId: { type: 'number', format: 'int32' },
      year: { type: 'number', format: 'int32', minimum: 1 },
      month: { type: 'number', format: 'int32', minimum: 1, maximum: 12 },
      locationId: { type: 'number', format: 'int32' },
      periodFrom: { type: 'string', format: 'date' },
      periodTo: { type: 'string', format: 'date' },
      consumption: { type: 'number', format: 'float', minimum: 0 },
      netCost: { type: 'number', format: 'float', minimum: 0 },
      vat: { type: 'number', format: 'float', minimum: 0, maximum: 100 },
      
    },
    required: ['itemId', 'year', 'month', 'locationId', 'netCost', 'vat'],
    additionalProperties: false
  },
  
  ExpenseUpdateRequest: {
    type: 'object',
    properties: {
      periodFrom: { type: 'string', format: 'date' },
      periodTo: { type: 'string', format: 'date' },
      consumption: { type: 'number', format: 'float', minimum: 0 },
      netCost: { type: 'number', format: 'float', minimum: 0 },
      vat: { type: 'number', format: 'float', minimum: 0, maximum: 100 },
    },
    additionalProperties: false
  }
  
};
