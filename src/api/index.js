const openApi = require('express-openapi');
const bodyParser = require('body-parser');
const apiDoc = require('./doc');
const errors = require('./errors');
const accessControlMiddleware = require('./acl');


let paths = [];
for(let e of ['user', 'expenses', 'reports', 'items']) {
  paths = paths.concat(require('./handlers/' + e));
}

function normalizeError(openApiError, schemaError) {
  return openApiError;
}

function errorMiddleware(err, req, res, next) {
  let errJson;
  
  if(err instanceof Error) {
    // [TODO] log this properly
    if(!(err instanceof errors.HttpError)) console.error(err);
    errJson = {
      isError: true,
      name: err.name,
      status: err.status || 500
    };
  } else {
    errJson = err;
    errJson.isError = true;
    if(!errJson.status) {
      errJson.status = 500;
    }
  }
  
  if(!errJson.name && errJson.status) {
    const errors = {
      '400': 'ValidationFailed'
    };
    
    if(errJson.status in errors) {
      errJson.name = errors[errJson.status];
    } else {
      if(errJson.status >= 500) {
        errJson.name = 'ServerError';
      }
    }
  }
  
  res.statusCode = errJson.status;
  res.json(errJson);
}

module.exports = function(app) {
  app.use(apiDoc.basePath, accessControlMiddleware);
  openApi.initialize({
    app,
    apiDoc,
    paths,
    errorMiddleware,
    consumesMiddleware: {
      'application/json': bodyParser.json()
    },
    errorTransformer: normalizeError,
  });
};
