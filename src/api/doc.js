module.exports = {
  swagger: '2.0',
  basePath: '/api',
  info: {
    title: 'Expense tracking API',
    version: '1.0.0'
  },
  consumes: ['application/json'],
  produces: ['application/json'],
  
  definitions: require('./definitions'),
  
  paths: {}
};
