const subclassError = require('subclass-error');

let e = {};

e.HttpError = subclassError('HttpError');
e.ServerError = subclassError('ServerError', e.HttpError, { status: 500 });
e.BadRequest = subclassError('BadRequest', e.HttpError, { status: 400 });
e.ValidationFailed = subclassError('ValidationFailed', e.HttpError, { status: 400 });
e.Unauthorized = subclassError('Unauthorized', e.HttpError, { status: 401 });
e.Forbidden = subclassError('Forbidden', e.HttpError, { status: 403 });
e.NotFound = subclassError('NotFound', e.HttpError, { status: 404 });

module.exports = e;
