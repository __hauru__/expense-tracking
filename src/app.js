const express = require('express');
const session = require('express-session');
const SessionFileStore = require('session-file-store')(session);
const path = require('path');
const middleware = require('./lib/middleware');

const config = require('config');

const initApi = require('./api');
const initAuth = require('./auth');
const initFrontend = require('./frontend');

let app = express();

app.disable('etag');
app.set('view engine', 'nunjucks');

app.use('/public/', express.static(path.join(__dirname, '../public/live/'), { fallthrough: true }));

app.use(middleware.noCache);

app.use(session({
  name: config.sessions.cookieName,
  secret: config.sessions.secret,
  store: new SessionFileStore({
    path: config.sessions.storePath,
    ttl: config.sessions.lifetime,
    retries: 1
  }),
  saveUninitialized: config.sessions.saveUninitialized,
  resave: false
}));

initAuth(app);
initApi(app);
initFrontend(app);

app.use(function(err, req, res, next) {
  console.error(err); // [TODO] log
  if(res.statusCode < 400) {
    res.status(500);
  }
  res.write(err.message || 'Error occured'); 
  res.end(); 
});

module.exports = app;


