module.exports = {
  
  roundPrice(price) {
    return Math.round(100 * price) / 100;
  }

};
