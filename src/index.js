require('dotenv').config();
require('console-stamp')(console, {
  pattern: 'yyyy-mm-dd HH:MM:ss'
});

const config = require('config');
let app = require('./app');

app.listen(config.port);

