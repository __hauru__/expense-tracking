const passport = require('passport');
const LocalStrategy = require('passport-local');
const Promise = require('bluebird');
const ldap = require('ldapjs');
const config = require('config');

const db = require('./db');

const User = db.model('User');

function authenticateWithLdap(login, password) {
  let client = ldap.createClient({
    url: config.ldap.serverUrl,
    strictDN: false
  });
  
  return new Promise(function(resolve, reject) {
    client.bind(config.ldap.dn + '\\' + login, password, function(err) {
      if(err) {
        if(err.name == 'InvalidCredentialsError') return resolve(null);
        return reject(err);
      }

      let searchOpts = {
        filter: '(sAMAccountName=' + login + ')', 
        scope: 'sub'
      }
      
      client.search(config.ldap.searchBase, searchOpts, function(err, res) {
        if(err) {
          return reject(err);
        }
        
        let results = [];
        
        res.on('searchEntry', function(entry) { results.push(entry.object); });
        
        res.on('end', function() {
          client.unbind(function(err) {
            if(err) console.warn('LDAP unbind failed!');
            resolve(results.length ? results[0] : null);
          });
        });
      });
    });
  });
}

function authenticate(login, password) {
  return User.findOne({ where: { login } })
    .then((user) => {
      if(user) {
        if(user.useLdapAuth) {
          return authenticateWithLdap(login, password)
            .then((result) => {
              return result ? user : null;
            });
        } else if(user.checkPassword(password)) {
          return user;
        }
      }
      return null; 
    })
    .tap((user) => {
      if(user) {
        return User.update({ lastAuthAt: db.literal('NOW()') }, { where: { id: user.id } });
      }
    })
    .then((user) => {
      return user;
    });
}

passport.use(new LocalStrategy({
  usernameField: 'login',
  passwordField: 'password'
}, function(login, password, done) {
  authenticate(login, password)
    .tap((user) => { done(null, user || false); })
    .catch((err) => { done(err); });
}));

passport.serializeUser(function(user, done) {
  done(null, { id: user.id, login: user.login });
});

passport.deserializeUser(function(data, done) {
  User.findById(data.id)
    .tap((user) => { done(null, user); })
    .catch((err) => { done(err); });
});

module.exports = function(app) {
  app.use(passport.initialize());
  app.use(passport.session());
};
