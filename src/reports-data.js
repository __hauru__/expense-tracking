const db = require('./db');
const Promise = require('bluebird');
const _ = require('lodash');
const Decimal = require('decimal');

const { Expense, Location, Item, Unit } = db.models;


function _createBlankArray(length, value) {
  let row = [];
  for(let i = 0; i < length; ++i) {
    row.push(_.clone(value));
  }
  
  return row;
}


function expensesByItem(year, itemId) {
  let filter = '', item;
  
  if(itemId) filter += ' AND item_id = ' + db.toInt(itemId);
  
  let query = `
    SELECT
      l.id AS locationId,
      l.name AS locationName,
      s.month AS month,
      s.net_cost AS netCost,
      s.gross_cost AS grossCost
    FROM locations AS l
    LEFT JOIN (
        SELECT 
          month,
          location_id,
          SUM(net_cost) AS net_cost,
          SUM(gross_cost) AS gross_cost
        FROM expenses
        WHERE year = ${ db.toInt(year) } ${ filter }
        GROUP BY month, location_id
        ORDER BY month ASC
      ) AS s ON s.location_id = l.id
    WHERE 1
    ORDER BY l.order ASC
  `;
  
  return (itemId ? Item.findById(itemId) : Promise.resolve(null))
    .then((inst) => { item = inst; return db.query(query) })
    .then(([rows, meta]) => {
      let result = {
        rows: [],
        monthTotals: _createBlankArray(12, [0, 0]),
        total: [0, 0],
        item: item ? item.serialize() : null
      };
      let resultRow, cell, lastId;
      
      for(let row of rows) {
        if(!resultRow || row.locationId != lastId) {
          resultRow = {
            locationName: row.locationName,
            expenses: _createBlankArray(12, [0, 0]),
            total: [0, 0]
          };
          result.rows.push(resultRow);
          lastId = row.locationId;
        }
        if(row.month) {
          cell = resultRow.expenses[row.month - 1];
          cell[0] = row.netCost || 0;
          cell[1] = row.grossCost || 0;
          resultRow.total[0] += cell[0] * 100;
          resultRow.total[1] += cell[1] * 100;
          result.monthTotals[row.month - 1][0] += cell[0] * 100;
          result.monthTotals[row.month - 1][1] += cell[1] * 100;
        }
      }
      
      for(resultRow of result.rows) {
        result.total[0] += resultRow.total[0];
        result.total[1] += resultRow.total[1];
        resultRow.total[0] /= 100;
        resultRow.total[1] /= 100;
      }
      
      result.total[0] /= 100;
      result.total[1] /= 100;
      
      for(let e of result.monthTotals) {
        e[0] /= 100;
        e[1] /= 100;
      }
      
      return result;
    });
}


function expensesByLocation(year, locationId) {
  let filter = '', location;
  
  if(locationId) filter += ' AND location_id = ' + db.toInt(locationId);
  
  let query = `
    SELECT
      i.id AS itemId,
      i.name AS itemName,
      s.month AS month,
      s.net_cost AS netCost,
      s.gross_cost AS grossCost
    FROM items AS i
    LEFT JOIN (
        SELECT 
          month,
          item_id,
          SUM(net_cost) AS net_cost,
          SUM(gross_cost) AS gross_cost
        FROM expenses
        WHERE year = ${ db.toInt(year) } ${ filter }
        GROUP BY month, item_id
        ORDER BY month ASC
      ) AS s ON s.item_id = i.id
    WHERE 1
    ORDER BY i.name ASC
  `;
  
  return (locationId ? Location.findById(locationId) : Promise.resolve(null))
    .then((inst) => { location = inst; return db.query(query) })
    .then(([rows, meta]) => {
      let result = {
        rows: [],
        monthTotals: _createBlankArray(12, [0, 0]),
        total: [0, 0],
        location
      };
      let resultRow, cell, lastId;
      
      for(let row of rows) {
        if(!resultRow || row.itemId != lastId) {
          resultRow = {
            itemName: row.itemName,
            expenses: _createBlankArray(12, [0, 0]),
            total: [0, 0]
          };
          result.rows.push(resultRow);
          lastId = row.itemId;
        }
        if(row.month) {
          cell = resultRow.expenses[row.month - 1];
          cell[0] = row.netCost || 0;
          cell[1] = row.grossCost || 0;
          resultRow.total[0] += cell[0] * 100;
          resultRow.total[1] += cell[1] * 100;
          result.monthTotals[row.month - 1][0] += cell[0] * 100;
          result.monthTotals[row.month - 1][1] += cell[1] * 100;
        }
      }
      
      for(resultRow of result.rows) {
        result.total[0] += resultRow.total[0];
        result.total[1] += resultRow.total[1];
        resultRow.total[0] /= 100;
        resultRow.total[1] /= 100;
      }
      
      result.total[0] /= 100;
      result.total[1] /= 100;
      
      for(let e of result.monthTotals) {
        e[0] /= 100;
        e[1] /= 100;
      }
      
      return result;
    });
}


function consumptionByItem(year, itemId) {
  let item;
  
  let query = `
    SELECT
      l.id AS locationId,
      l.name AS locationName,
      s.month AS month,
      s.consumption AS consumption
    FROM locations AS l
    LEFT JOIN (
        SELECT 
          month,
          location_id,
          COALESCE(SUM(consumption), 0) AS consumption
        FROM expenses
        WHERE year = ${ db.toInt(year) } AND item_id = ${ db.toInt(itemId) }
        GROUP BY month, location_id
        ORDER BY month ASC
      ) AS s ON s.location_id = l.id
    WHERE 1
    ORDER BY l.order ASC
  `;
  
  return Item.findById(itemId, { include: [{ model: Unit, as: 'unit' }] })
    .then((inst) => { item = inst; return db.query(query) })
    .then(([rows, meta]) => {
      let result = {
        rows: [],
        monthTotals: _createBlankArray(12, Decimal(0)),
        total: Decimal(0),
        item: item.serialize()
      };
      let resultRow, cell, lastId, row;
      
      for(row of rows) {
        if(!resultRow || row.locationId != lastId) {
          resultRow = {
            locationName: row.locationName,
            values: _createBlankArray(12, 0),
            total: Decimal(0)
          };
          result.rows.push(resultRow);
          lastId = row.locationId;
        }
        if(row.month) {
          resultRow.values[row.month - 1] = row.consumption || 0;
          resultRow.total = resultRow.total.add(row.consumption);
          result.monthTotals[row.month - 1] = result.monthTotals[row.month - 1].add(row.consumption);
        }
      }
      
      for(resultRow of result.rows) {
        result.total = result.total.add(resultRow.total);
      }
      
      // convert to regular floats
      for(row of result.rows) {
        row.total = row.total.toNumber();
      }
      
      result.monthTotals = result.monthTotals.map(n => n.toNumber());
      result.total = result.total.toNumber();
      
      return result;

    });
}

module.exports = {
  expensesByItem,
  expensesByLocation,
  consumptionByItem,
};







