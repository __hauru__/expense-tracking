const nj = require('nunjucks');
const path = require('path');
const Promise = require('bluebird');
const config = require('config');

const db = require('./db');

const Location = db.model('Location');
const Item = db.model('Item');


module.exports = function(app) {
  let env = nj.configure(path.join(__dirname, 'views'), {
    express: app,
    autoescape: false
  });
  
  env.addGlobal('baseUrl', config.baseUrl);
  env.addGlobal('version', config.version);
  
  app.get('/', function(req, res, next) {
    if(req.user) {
      return res.redirect(config.baseUrl + '/app');
    }
    res.render('login-form.html');
  });
  
  app.get('/app', function(req, res, next) {
    if(!req.user) {
      return res.redirect(config.baseUrl + '/');
    }
    
    let years = [], currentYear = (new Date()).getFullYear();
    for(let i = 2011; i <= currentYear + 1; ++i) years.push(i);
    
    Promise.all([
      Location.getAll(),
      Item.scope('withUnit', 'ordered').findAll()
    ])
    .then(([locations, items]) => {
      res.render('main.html', {
        user: req.user.serialize(),
        years,
        locations: Location.serializeMany(locations),
        items: Item.serializeMany(items)
      });
      
    })
    .catch((err) => { next(err); });
  });
  
  app.use('/reports/', function(req, res, next) {
    if(!req.user) {
      return res.redirect(config.baseUrl + '/');
    }
    
    let m = req.path.match(/^\/([\w\-]*)$/);
    if(m) {
      let reportName = m[1];
      try {
        return res.render(`reports/${ reportName }.html`, {
          query: req.query
        });
      } catch(err) {
        console.error(err);
        return next();
      }
    }
    
    next();
  });
  
};
