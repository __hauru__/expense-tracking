const readlineSync = require('readline-sync');
const db = require('../db');

const User = db.model('User');

let data = { useLdapAuth: false };

data.login = readlineSync.question('Login: ');
data.password = readlineSync.question('Password: ', { hideEchoBack: true });
data.fullName = readlineSync.question('Full name: ');

let user = User.build(data);
user.setPassword(data.password);

user.save()
.then(function() {
  console.log('User created!');
  process.exit();
})
.catch(function(err) {
  if(err.name == 'SequelizeUniqueConstraintError') {
    console.error('ERROR: User "%s" already exists', data.login);
  } else {
    console.error(err);
  }
  process.exit(1);
});





