const db = require('../db');

const options = {
  force: false
};

db.sync(options)
.then(() => {
  console.log('Data tables created!');
  process.exit();
})
.catch((err) => {
  console.error(err);
  process.exit(1);
});

