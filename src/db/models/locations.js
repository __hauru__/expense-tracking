module.exports = function(db, DataTypes) {

  let Location = db.define('Location', {
  
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    
    name: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    
    order: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  
  }, {
    
    tableName: 'locations',
    createdAt: false,
    updatedAt: false,
    
    indexes: [
      { name: 'locations_order', fields: ['order', 'name'], unique: false }
    ],
    
    classMethods: {
      
      getAll(options) {
        options = Object.assign({}, options, {
          where: null,
          order: [['order', 'ASC'], ['name', 'ASC']]
        });
        
        return this.findAll(options);
      },
      
      serializer: {
        schemes: {
          default: { include: ['id', 'name'] }
        }
      }
      
    }
  });
};
