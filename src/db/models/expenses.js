const { roundPrice } = require('../../lib/util');

function updateGrossCost(inst) {
  inst.updateGrossCost();
}

module.exports = function(db, DataTypes) {
  
  let Unit = db.model('Unit');
  let Item = db.model('Item');
  
  let Expense = db.define('Expense', {
    
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    
    itemId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'item_id'
    },
    
    year: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    
    month: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    
    locationId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'location_id'
    },
    
    periodFrom: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      field: 'period_from',
    },
    
    periodTo: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      field: 'period_to',
    },
    
    consumption: {
      type: DataTypes.DECIMAL(12, 6),
      allowNull: true,
    },
    
    netCost: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
      field: 'net_cost'
    },
    
    vat: {
      type: DataTypes.DECIMAL(4, 2),
      allowNull: false
    },
    
    grossCost: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
      field: 'gross_cost'
    },
    
    notes: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'created_at'
    },
    
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'updated_at'
    },
    
    lastEditedById: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'last_edited_by_id'
    }
    
  }, {
    tableName: 'expenses',
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    
    indexes: [
      { name: 'expenses_year_month', fields: ['year', 'month'] },
      { name: 'expenses_location_id', fields: ['location_id'] },
      { name: 'expenses_item_id', fields: ['item_id'] },
    ],
    
    classMethods: {
      serializer: {
        schemes: {
          'default': { include: ['@all'] },
          'list': { include: ['@all', 'item'] }
        }
      }
    },
    
    instanceMethods: {
      updateGrossCost() {
        this.grossCost = roundPrice(this.netCost * (1 + this.vat / 100));
      }
    },
    
    hooks: {
      beforeValidate: updateGrossCost,
      beforeUpdate: updateGrossCost,
    }
  });
  
};
