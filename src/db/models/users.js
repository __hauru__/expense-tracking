const bcrypt = require('bcryptjs');

module.exports = function(db, DataTypes) {

  let User = db.define('User', {
  
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    
    login: {
      type: DataTypes.STRING(128),
      allowNull: false
    },
    
    password: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    
    fullName: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'full_name'
    },
    
    useLdapAuth: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
      field: 'use_ldap_auth'
    },
    
    lastAuthAt: {
      type: DataTypes.DATE,
      field: 'last_auth_at'
    }
  
  }, {
    
    tableName: 'users',
    createdAt: false,
    updatedAt: false,
    
    indexes: [
      { name: 'users_login', unique: true, fields: ['login'] }
    ],
    
    classMethods: {
      serializer: {
        schemes: {
          default: { include: ['id', 'login', 'fullName'] }
        }
      }
    },
    
    instanceMethods: {
      
      setPassword(password) {
        this.password = bcrypt.hashSync(password, 12);
      },
      
      checkPassword(password) {
        return bcrypt.compareSync(password, this.password);
      }
      
    },
    
    
  });

};
