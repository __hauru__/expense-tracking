module.exports = function(db, DataTypes) {

  let Unit = db.define('Unit', {
  
    symbol: {
      type: DataTypes.STRING(8),
      primaryKey: true,
      allowNull: false
    },
    
    name: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    
    symbolHtml: {
      type: DataTypes.STRING(16),
      allowNull: false,
      field: 'symbol_html'
    },
  
  }, {
    
    tableName: 'units',
    underscored: true,
    
  });
  
  let Item = db.define('Item', {
    
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    
    name: {
      type: DataTypes.STRING(96),
      allowNull: false
    },
    
    type: {
      type: DataTypes.ENUM('water', 'electricity', 'heating'),
      allowNull: true
    },
    
    flatRate: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      field: 'flat_rate'
    },
    
    unitSymbol: {
      type: DataTypes.STRING(8),
      allowNull: true,
      field: 'unit_symbol'
    },
    
    multiplier: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    }
    
  }, {
    
    tableName: 'items',
    indexes: [
      { name: 'items_type', fields: ['type'] },
      { name: 'items_name', fields: ['name'] }
    ],
    
    classMethods: {
      serializer: {
        schemes: {
          default: {
            include: ['@all', 'unit']
          }
        }
      },
    }
    
  });
};

