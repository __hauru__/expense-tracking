module.exports = function(db) {
  
  let {
    User,
    Location,
    Unit,
    Item,
    Expense
  } = db.models;
  
  Item.addScope('withUnit', {
    include: { model: Unit, as: 'unit' }
  });
  
  Item.addScope('ordered', {
    order: [['name', 'ASC']]
  });
  
  Expense.addScope('withItem', {
    include: { model: Item.scope('withUnit'), as: 'item' }
  });
  
};

