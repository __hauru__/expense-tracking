const config = require('config');
const Sequelize = require('sequelize');
const path = require('path');
const Serializer = require('sequelize-to-json');

const addRelations = require('./relations'), addScopes = require('./scopes');

Serializer.defaultOptions.attrFilter = (a) => !a.fieldName.includes('_');

let db = new Sequelize(config.db.name, config.db.user, config.db.password, {
  host: config.db.host,
  port: config.db.port,
  logging: config.db.logging,
  
  define: {
    createdAt: false,
    updatedAt: false,
    underscored: true,
    
    classMethods: {
      serializeMany: function(data, scheme, options) {
        return Serializer.serializeMany(data, this, scheme, options);
      }
    },
    
    instanceMethods: {
      serialize: function(scheme, options) {
        return (new Serializer(this.Model, scheme, options)).serialize(this);
      }
    }
  },

});

db.toInt = function(val) {
  let result = 1 * val;
  return isNaN(result) ? 'NULL' : Math.floor(result);
};

const MODEL_FILES = [
  'users',
  'locations',
  'items',
  'expenses'
];

for(let file of MODEL_FILES) {
  db.import(path.join(__dirname, 'models', file + '.js'));
}

addRelations(db);
addScopes(db);

module.exports = db;
