module.exports = function(db) {
  
  let {
    User,
    Location,
    Unit,
    Item,
    Expense
  } = db.models;
  
  Item.belongsTo(Unit, { as: 'unit', foreignKey: 'unit_symbol', targetKey: 'symbol' });
  
  Expense.belongsTo(Location, { as: 'location', foreignKey: 'location_id' });
  Expense.belongsTo(Item, { as: 'item', foreignKey: 'item_id' });
  Expense.belongsTo(User, { as: 'lastEditedBy', foreignKey: 'last_edited_by_id' });
  
};
