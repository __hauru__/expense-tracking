var App = new (function() {
  
  var App = this;
  var templates = {};
  
  App.getTemplate = function(name) {
    if(templates[name]) {
      return templates[name];
    }

    var el = document.querySelector('script[type="text/x-vue"][name="' + name + '"]');

    if(el) {
      var html = el.innerHTML;
      if(!el.hasAttribute('data-nowrap')) {
        html = '<div class="' + name + '-component">' + html + '</div>';
      }
      
      templates[name] = html;
      return html;

    } else {
      throw new Error('Template not found: ' + name);
    }
  };
  
  App.leftPad = function(s, l, char) {
    s = '' + s;
    var d = l - s.length, p = '';
    while(d > 0) { p += char; --d; }
    return p + s;
  };
  
  App.formatDate = function(date) {
    return date.getFullYear() 
      + '-' + App.leftPad(date.getMonth() + 1, 2, '0') 
      + '-' + App.leftPad(date.getDate(), 2, '0');
  };

  App.findBy = function(list, key, value) {
    var i = 0, e;
    for( ; i < list.length; ++i) {
      e = list[i];
      if(e[key] == value) {
        return e;
      }
    }
    
    return null;
  };
  
  App.roundPrice = function(price) {
    return Math.round(100 * price) / 100;
  };
  
  App.removeEmpty = function(data) {
    var toRemove = [];
    for(var key in data) {
      if(data[key] === null || data[key] === '') {
        toRemove.push(key);
      }
    }
    
    for(var i = 0; i < toRemove.length; ++i) delete data[toRemove[i]];
  };
  
  App.triggerEvent = function(target, event) {
    if(document.createEvent) {
      if(typeof(event) == 'string') {
        event = new Event(event);
      }
      target.dispatchEvent(event);
      
    } else {
      target.fireEvent('on' + event, document.createEventObject());
    }
  };
  
  App.insertSeparators = function(str, n, sep) {
    str = '' + str;
    sep = sep || ' ';
    var r = str.length % n, result = str.substr(0, r), pos = r;
    
    while(pos < str.length) {
      result += (result ? sep : '') + str.substr(pos, n);
      pos += n;
    }
    
    return result;
  };
  
  App.formatPrice = function(price, nbsp) {
    var s = price >= 0 ? 1 : -1;
    price *= s;
    return App.insertSeparators(s * Math.floor(price), 3, nbsp === false ? ' ' : '&nbsp;') 
      + ',' + App.leftPad(Math.floor(price * 100) % 100, 2, '0');
  };
  
  App.removeBy = function(list, key, value) {
    var i = 0, found = false;
    for( ; i < list.length; ++i) {
      if(list[i][key] === value) {
        found = true;
        break;
      }
    }
    
    if(found) {
      list.splice(i, 1);
    }
  }

})();
