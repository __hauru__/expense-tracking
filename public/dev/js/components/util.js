Vue.component('form-input', {
  template: App.getTemplate('form-input'),
  props: ['error'],
    
  mounted: function() {
    var el = this.getInputElement();
    if(el) {
      el.addEventListener('input', this.onEditInput, true);
      el.addEventListener('change', this.onEditInput, true);
    }
  },
  
  methods: {
    getInputElement: function() {
      var node = this.$slots.default[0];
      if(node) {
        var $el = $(node.elm);
        if($el.is(':input:visible')) {
          return node.elm;
        } else {
          var $input = $el.find(':input:visible');
          if($input.length) {
            return $input[0];
          }
        }
      }
      
      return null;
    },
    
    onEditInput: function(event) {
      this.$emit('changed', event.currentTarget.name, event.currentTarget);
    }
  }
});

