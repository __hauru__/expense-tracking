Vue.component('expenses-by-item-report', {
  template: App.getTemplate('expenses-by-item-report'),
  props: ['year', 'itemId', 'amountType'],
  data: function() {
    return {
      reportData: null,
      item: null,
      needsUpdate: false
    };
  },
  
  watch: {
    year: 'invalidate',
    itemId: 'invalidate',
    needsUpdate: function(newVal, oldVal) {
      if(newVal) this.update();
    }
  },
  
  mounted: function() {
    this.invalidate();
  },
  
  computed: {
    App: function() {
      return App;
    },
    amountIndex: function() {
      return this.amountType;
    }
  },
  
  methods: {
    invalidate: function() {
      //alert('invalidate');
      this.needsUpdate = true;
    },
    
    update: function() {
      //alert('update ' + this.year + ' ' + this.itemId);
      var self = this;
      var params = { year: this.year };
      
      if(this.itemId) {
        params.itemId = this.itemId;
      }
      
      axios.request({ 
          method: 'GET',
          url: 'api/reports/expenses-by-item',
          params: params
      })
      .then(function(response) {
        self.reportData = response.data;
        self.item = response.data.item;
      })
      .catch(function(err) {
        self.reportData = null;
        self.item = null
      })
      .then(function() {
        self.needsUpdate = false;
      });
    }
  }
  
});


Vue.component('expenses-by-location-report', {
  template: App.getTemplate('expenses-by-location-report'),
  props: ['year', 'locationId', 'amountType'],
  data: function() {
    return {
      reportData: null,
      location: null,
      needsUpdate: false
    };
  },
  
  watch: {
    year: 'invalidate',
    locationId: 'invalidate',
    needsUpdate: function(newVal, oldVal) {
      if(newVal) this.update();
    }
  },
  
  mounted: function() {
    this.invalidate();
  },
  
  computed: {
    App: function() {
      return App;
    },
    amountIndex: function() {
      return this.amountType;
    }
  },
  
  methods: {
    invalidate: function() {
      //alert('invalidate');
      this.needsUpdate = true;
    },
    
    update: function() {
      //alert('update ' + this.year + ' ' + this.itemId);
      var self = this;
      var params = { year: this.year };
      
      if(this.locationId) {
        params.locationId = this.locationId;
      }
      
      axios.request({ 
          method: 'GET',
          url: 'api/reports/expenses-by-location',
          params: params
      })
      .then(function(response) {
        self.reportData = response.data;
        self.location = response.data.location;
      })
      .catch(function(err) {
        self.reportData = null;
        self.location = null
      })
      .then(function() {
        self.needsUpdate = false;
      });
    }
  }
  
});


Vue.component('consumption-by-item-report', {
  template: App.getTemplate('consumption-by-item-report'),
  props: ['year', 'itemId', 'amountType'],
  data: function() {
    return {
      reportData: null,
      item: null,
      needsUpdate: false
    };
  },
  
  watch: {
    year: 'invalidate',
    itemId: 'invalidate',
    needsUpdate: function(newVal, oldVal) {
      if(newVal) this.update();
    }
  },
  
  mounted: function() {
    this.invalidate();
  },
  
  computed: {
    App: function() {
      return App;
    }
  },
  
  methods: {
    invalidate: function() {
      this.needsUpdate = true;
    },
    
    update: function() {
      var self = this;
      var params = { year: this.year, itemId: this.itemId };
      
      axios.request({ 
          method: 'GET',
          url: 'api/reports/consumption-by-item',
          params: params
      })
      .then(function(response) {
        self.reportData = response.data;
        self.item = response.data.item;
      })
      .catch(function(err) {
        self.reportData = null;
        self.item = null
      })
      .then(function() {
        self.needsUpdate = false;
      });
    }
  }
  
});
