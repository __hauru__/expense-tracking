
(function() {

  function createBlankData(inst) {
    var d = new Date(inst.year, inst.month - 1, 1);
      
    var prevMonth = d.getMonth() - 1;
    if(prevMonth < 0) prevMonth = 11;
    
    var d1 = new Date(prevMonth == 11 ? d.getFullYear() - 1 : d.getFullYear(), prevMonth, 1);
    var d2 = new Date(d.getTime() - 24 * 3600 * 1000);
    
    return {
      expenseData: {
        year: inst.year,
        month: inst.month,
        locationId: inst.locationId,
        itemId: '',
        periodFrom: App.formatDate(d1),
        periodTo: App.formatDate(d2),
        vat: 23,
        consumption: null,
        netCost: null,
        notes: null
      },
      errors: {
        itemId: null,
        periodFrom: null,
        periodTo: null,
        vat: null,
        consumption: null,
        netCost: null,
        taxPercent: null
      }
    };
  }
  
  Vue.component('edit-expense', {
    template: App.getTemplate('edit-expense'),
    props: ['id', 'year', 'month', 'locationId', 'items'],
    components: {
      'datepicker': VueStrap.datepicker
    },
    
    data: function() {
      return createBlankData(this);
    },
    
    computed: {
      grossCost: function() {
        var data = this.expenseData, netCost = 1 * data.netCost, vat = 1 * data.vat;
        if(!isNaN(netCost) && !isNaN(vat)) {
          return App.roundPrice(netCost * (1 + vat / 100));
        } else {
          return null;
        }
      },
      selectedItem: function() {
        if(this.expenseData.itemId) {
          return App.findBy(this.items, 'id', 1 * this.expenseData.itemId);
        } else {
          return null;
        }
      },
      addingNew: function() {
        return !this.id;
      }
    },
    
    mounted: function() {
      var self = this, $el = $(this.$el);
      $el.find('input[name="netCost"]')[0].addEventListener('change', function() {
        self.expenseData.netCost = App.roundPrice(self.expenseData.netCost);
      });
      $el.find('input[name="vat"]')[0].addEventListener('change', function() {
        self.expenseData.vat = App.roundPrice(self.expenseData.vat);
      });
      
      if(this.id) {
        this.loadData();
      }
      
    },
    
    watch: {
      id: function(id) {
        if(id) this.loadData();
        else this.reset();
      }
    },
    
    methods: {
      loadData: function() {
        if(!this.id) return;
        
        var self = this;
      
        axios.request({ method: 'GET', url: 'api/expenses/' + this.id })
        .then(function(response) {
          var data = _.pick(response.data, ['year', 'month', 'locationId', 'itemId', 'periodFrom', 'periodTo', 
            'netCost', 'vat', 'consumption', 'notes']);
          _.assign(self.$data.expenseData, data);
        })
        .catch(function(err) {
          console.error(err);
        });
      },
      
      cancel: function() {
        this.$emit('cancel');
      },
      
      onClickVatBtn: function(event) {
        event.preventDefault();
        this.expenseData.vat = 1 * event.target.getAttribute('data-value');
        App.triggerEvent($('input[name="vat"]', this.$el)[0], 'change');
      },
      
      submit: function() {
        var update = !!this.id;
        var data = _.cloneDeep(this.expenseData), url
        var self = this;
        App.removeEmpty(data);
        
        if(update) {
          delete data.year;
          delete data.month;
          delete data.locationId;
          delete data.itemId;
          url = 'api/expenses/' + this.id + '/update';
        } else {
          url = 'api/expenses/create';
        }
        
        var errors = {
          'itemId': 'Nie wybrano artykułu',
          'netCost': 'Nieprawidłowa cena',
          'periodFrom': 'Nieprawidłowa data',
          'periodTo': 'Nieprawidłowa data'
        };
        
        axios.request({ method: 'POST', url: url, data: data })
        .then(function(response) {
          if(update) {
            self.$emit('expense-updated', self.id);
          } else {
            self.reset();
            self.$emit('expense-created', response.data.id);
          }
        })
        .catch(function(err) {
          if(err.response && err.response.status == 400) {
            var data = err.response.data, hasErrors = false;
            if(data && data.errors) {
              data.errors.forEach(function(e) {
                self.errors[e.path] = errors[e.path] || 'Nieprawidłowa wartość';
                if(!hasErrors && e.path != 'periodFrom' && e.path != 'periodTo') {
                  hasErrors = true;
                }
              });
            }
            self.$emit('form-error', hasErrors);
          } else {
            console.error(err);
          }
        });
      },
      
      clearError: function(name) {
        this.errors[name] = null;
      },
      
      reset: function() {
        var blank = createBlankData(this);
        _.merge(this.$data, blank);
      }
    }
  });
})();

Vue.component('expenses-list', {
  template: App.getTemplate('expenses-list'),
  props: ['year', 'month', 'locationId', 'highlightId'],
  data: function() {
    return {
      expenses: [],
      loading: true
    };
  },
  mounted: function() {
    this.reload();
  },
  watch: {
    year: 'reload',
    month: 'reload',
    locationId: 'reload'
  },
  computed: {
    App: function() {
      return App;
    },
    summary: function() {
      var result = { netCost: 0, grossCost: 0 };
      for(var i = 0; i < this.expenses.length; ++i) {
        result.netCost += 100 * this.expenses[i].netCost;
        result.grossCost += 100 * this.expenses[i].grossCost;
      }
      
      result.netCost /= 100;
      result.grossCost /= 100;
      return result;
    }
  },
  methods: {
    reload: function(loading) {
      var self = this;
      if(loading !== false) this.loading = true;
      axios.request({ 
        method: 'GET',
        url: 'api/expenses', 
        params: { year: this.year, month: this.month, locationId: this.locationId }
      })
      .then(function(response) {
        self.expenses = response.data;
      })
      .catch(function(err) {
        console.error(err);
      })
      .then(function() {
        if(self.loading) self.loading = false;
      });
    },
    
    onClickAction: function(event) {
      var $a = $(event.target).closest('a'), action = $a.attr('data-action');
      var id = 1 * $a.closest('.actions').attr('data-id');
      var expense = _.cloneDeep(App.findBy(this.expenses, 'id', id));
      
      if(action == 'edit') this.$emit('request-edit', expense);
      else if(action == 'remove') this.$emit('request-remove', expense);
    }
  }
});

Vue.component('expenses-view', {
  template: App.getTemplate('expenses-view'),
  props: ['globals'],
  mixins: [MessagesMixin],
  data: function() {
    var now = new Date();
    return {
      params: {
        currentYear: now.getFullYear(),
        currentMonth: now.getMonth() + 1,
        currentLocationId: this.globals.locations[0].id,
      },
      expenseFormVisible: false,
      editedExpenseId: null
    };
  },
  
  computed: {
    currentLocation: function() {
      return App.findBy(this.globals.locations, 'id', 1 * this.params.currentLocationId);
    },
    currentMonthName: function() {
      return this.globals.months[1 * this.params.currentMonth - 1];
    }
  },
  
  mounted: function() {
    var $el = $(this.$el);
    for(var key in this.params) {
      $el.find('select[name="' + key + '"]').val(this.params[key]);
    }
  },
  
  methods: {
    
    onClickAdd: function() {
      this.showExpenseForm();
    },
    
    onExpenseCreated: function() {
      this.showMessage('success', 'Rekord został dodany');
      this.reloadList(false);
    },
    
    onExpenseUpdated: function() {
      this.showMessage('success', 'Zmiany zostały zapisane');
      this.hideExpenseForm();
      this.reloadList(false);
    },
    
    onFormError: function(formHasErrors) {
      if(!formHasErrors) this.showMessage('error', 'Błędne dane w formularzu');
    },
    
    onChangeParams: function(event) {
      var $select = $(event.target), name = $select.attr('name'), val = $select.val();
      if(val != this.params[name]) {
        this.hideExpenseForm();
        this.params[name] = 1 * val;
      }
      
    },
    
    showExpenseForm: function(id) {
      this.editedExpenseId = id || null;
      this.expenseFormVisible = true;
    },
    
    hideExpenseForm: function() {
      this.expenseFormVisible = false;
      this.editedExpenseId = null;
    },
    
    reloadList: function(loading) {
      this.$refs.list.reload(loading);
    },
    
    removeExpense: function(inst) {
      if(!confirm('Na pewno usunąć rekord "' + inst.item.name + '"?')) return;
      
      var self = this;
      
      axios.request({ method: 'POST', url: 'api/expenses/' + inst.id + '/delete' })
      .then(function(response) {
        self.reloadList(false);
        self.showMessage('success', 'Rekord został usunięty');
      })
      .catch(function(err) {
        console.error(err);
      })
      .then(function() {
        if(self.editedExpenseId == inst.id) {
          self.editedExpenseId = null;
        }
      });
      
    },
    
    editExpense: function(inst) {
      this.showExpenseForm(inst.id);
      smoothScroll.animateScroll(0);
    }
  
  }
});
