var eventBus = new Vue();

var MessagesMixin = {
  methods: {
    showMessage: function(type, text) {
      eventBus.$emit('message', type, text);
    }
  }
};

Vue.component('global-messages', {
  template: App.getTemplate('global-messages'),
  data: function() {
    return {
      messages: [],
      lastMessageId: 0
    };
  },
  
  computed: {
    messagesReversed: function() {
      var l = [];
      for(var i = this.messages.length; i > 0; --i) l.push(this.messages[i - 1]);
      return l;
    }
  },
  
  mounted: function() {
    eventBus.$on('message', this.addMessage.bind(this));
  },
  
  methods: {
    addMessage: function(type, text) {
      var id = ++this.lastMessageId;
      if(type == 'error') type = 'danger';
      var icons = {
        'success': 'ok',
        'danger': 'remove',
        'warning': 'exclamation-sign',
        'info': 'info-sign'
      };
      
      this.messages.push({
        id: id,
        type: type,
        text: text,
        icon: icons[type]
      });
      
      var self = this;
      setTimeout(function() {
        self.hideMessage.call(self, id);
      }, 3000);
    },
    
    hideMessage: function(id) {
      var self = this;
      $(this.$el).find('.message[data-id="' + id + '"]').addClass('message-hidden');
      setTimeout(function() {
        self.removeMessage.call(self, id);
      }, 200);
    },
    
    removeMessage: function(id) {
      App.removeBy(this.messages, 'id', id);
    }
  }
  
});
