Vue.component('expenses-by-item-report-view', {
  template: App.getTemplate('expenses-by-item-report-view'),
  props: ['globals'],
  data: function() {
    var now = new Date();
    return {
      params: {
        year: null,
        itemId: null,
        amountType: null,
      },
      selected: {
        year: now.getFullYear() - 1,
        itemId: null,
        amountType: 1
      },
      reportVisible: false,
      
    };
  },
  
  methods: {
    showReport: function() {
      this.params.year = this.selected.year;
      this.params.itemId = this.selected.itemId;
      this.params.amountType = this.selected.amountType;
      this.reportVisible = true;
    }
  },
  
  computed: {
    printableVersionHref: function() {
      var url = this.globals.baseUrl + '/reports/expenses-by-item?year=' + this.selected.year + '&amountType=' + this.selected.amountType;
      if(this.selected.itemId) {
        url += '&itemId=' + this.selected.itemId;
      }
      
      return url;
    }
  }
  
});


Vue.component('expenses-by-location-report-view', {
  template: App.getTemplate('expenses-by-location-report-view'),
  props: ['globals'],
  data: function() {
    var now = new Date();
    return {
      params: {
        year: null,
        locationId: null,
        amountType: null,
      },
      selected: {
        year: now.getFullYear() - 1,
        locationId: null,
        amountType: 1
      },
      reportVisible: false,
      
    };
  },
  
  methods: {
    showReport: function() {
      this.params.year = this.selected.year;
      this.params.locationId = this.selected.locationId;
      this.params.amountType = this.selected.amountType;
      this.reportVisible = true;
    }
  },
  
  computed: {
    printableVersionHref: function() {
      var url = this.globals.baseUrl + '/reports/expenses-by-location?year=' + this.selected.year + '&amountType=' + this.selected.amountType;
      if(this.selected.locationId) {
        url += '&locationId=' + this.selected.locationId;
      }
      
      return url;
    }
  }
  
});


Vue.component('consumption-by-item-report-view', {
  template: App.getTemplate('consumption-by-item-report-view'),
  props: ['globals'],
  //mixins: [MessagesMixin],
  data: function() {
    var now = new Date();
    return {
      params: {
        year: null,
        itemId: null
      },
      selected: {
        year: now.getFullYear() - 1,
        itemId: null
      },
      reportVisible: false,
    };
  },
  
  methods: {
    showReport: function() {
      //if(!this.selected.itemId) {
        //this.showMessage('error', 'Wybierz artykuł, którego ma dotyczyć raport');
        //this.reportVisible = false;
        //return;
      //}
      
      this.params.year = this.selected.year;
      this.params.itemId = this.selected.itemId;
      this.params.amountType = this.selected.amountType;
      this.reportVisible = true;
    }
  },
  
  computed: {
    printableVersionHref: function() {
      var url = this.globals.baseUrl + '/reports/consumption-by-item?year=' + this.selected.year + '&itemId=' + this.selected.itemId;
      return url;
    }
  }
  
});
