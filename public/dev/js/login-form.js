@@include('common/app-util.js')
@@include('components/util.js')

Vue.component('login-form', {
  template: App.getTemplate('login-form'),
  
  data: function() {
    return {
      errorMessage: null,
      locked: false,
      formData: {
        login: '',
        password: '',
      },
      formErrors: {
        login: '',
        password: '',
      }
    };
  },
  
  methods: {
    
    logIn: function() {
      var self = this;
      
      this.formErrors.login = '';
      this.formErrors.password = '';
      this.errorMessage = '';
      
      if(!this.formData.login || !this.formData.password) {
        if(!this.formData.login) {
          this.formErrors.login = 'Proszę wpisać login';
        }
        if(!this.formData.password) {
          this.formErrors.password = 'Proszę wpisać hasło';
        }
        
        return;
      }
      
      this.locked = true;
      axios.request({
        url: 'api/user/log-in',
        method: 'post',
        data: this.formData
      })
      .then(function(response) {
        window.location = Globals.baseUrl + '/app';
      })
      .catch(function(err) {
        if(err.response && err.response.status == 401) {
          self.errorMessage = 'Nieprawidłowa nazwa użytkownika i/lub hasło';
        } else {
          self.errorMessage = 'Wystąpił błąd';
          console.log(err.response);
        }
        self.locked = false;
      });
    },
    
    clearError: function(name) {
      this.formErrors[name] = '';
    },
    
    focusOnPassword: function() {
      $(this.$el).find('input[name="password"]').focus();
    }
  
  }
  
});
