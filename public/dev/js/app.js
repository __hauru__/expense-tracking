@@include('common/app-util.js')
@@include('components/util.js')
@@include('components/messages.js')
@@include('components/expenses-view.js')
@@include('components/reports-content.js')
@@include('components/reports.js')

axios.defaults.validateStatus = function(status) {
  if(status == 401) {
    window.location = '/';
    return;
  }
  return status >= 200 && status < 300;
};

smoothScroll.init({ speed: 200 });

new Vue({
  el: '#app',
  components: {
    'dropdown': VueStrap.dropdown,
    'navbar': VueStrap.navbar,
  },
  data: {
    currentView: 'expenses-view'
  },
  computed: {
    user: function() {
      return Globals.user;
    },
    years: function() {
      return Globals.years;
    },
    locations: function() {
      return Globals.locations;
    },
    globals: function() {
      return Globals;
    }
  },
  
  mounted: function() {
    this.updateNav();
  },
  
  methods: {
    onClickNav: function(event) {
      var el = event.target;
      if(el.tagName.toLowerCase() == 'a' && el.hasAttribute('data-view')) {
        var view = el.getAttribute('data-view');
        if(this.currentView != view) {
          this.currentView = view;
          this.updateNav();
        }
      }
    },
    
    updateNav: function() {
      var self = this;
      $(this.$el).find('.navbar-nav a[data-view]').each(function() {
        var $el = $(this), view = $el.attr('data-view');
        if(view == self.currentView) {
          $el.parentsUntil('.navbar-nav', 'li').addClass('active');
        } else {
          $el.parentsUntil('.navbar-nav', 'li.active').removeClass('active');
        }
      });
      
    },
    
    
    logOut: function() {
      var self = this;
      axios.request({
        url: 'api/user/log-out',
        method: 'post'
      })
      .then(function(response) {
        window.location = self.globals.baseUrl + '/';
      })
      .catch(function(err) {
        console.log(err);
      });
    }
  }
});

