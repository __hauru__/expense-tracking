@@include('common/app-util.js')
@@include('components/reports-content.js')


new Vue({
  el: '#app',
  methods: {
    modifyFontSize: function(value) {
      var $body = $('body');
      var fs = $body.css('font-size');
      $body.css('font-size', (parseInt(fs) + value) + 'px');
    },
    
    enlargeFont: function() {
      this.modifyFontSize(1);
    },
    
    shrinkenFont: function() {
      this.modifyFontSize(-1);
    },
    
    showPrintDialog: function() {
      window.print();
    }
  }
});
