@@if(forProduction) {
  @@include('vendor/vue.min.js')
}

@@if(!forProduction) {
  @@include('vendor/vue.js')
}

@@include('vendor/vue-strap.min.js')
@@include('vendor/jquery.slim.min.js')
@@include('vendor/lodash.min.js')
@@include('vendor/smooth-scroll.min.js')
