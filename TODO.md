# TODO

Ważne:

- ECONNRESET nie wiadomo skąd
- Naprawić: zachowanie przy błędnym cookie sesji
- multiplier używać


Mniej ważne:

- Przejść na zapisywanie kwot w groszach
- Komunikaty błędów
- Dodać do specyfikacji API schematy odpowiedzi (200)
- Dodać do specyfikacji API schemat błędu walidacji
- Polskie napisy w datepickerze

