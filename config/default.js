const env = process.env;

module.exports = {
  version: '20171205.1',
  port: env.PORT,
  baseUrl: (env.BASE_URL || '').replace(/\/+$/, ''),
  db: {
    host: env.DB_HOST || 'localhost',
    port: env.DB_PORT || 3306,
    name: env.DB_NAME,
    user: env.DB_USER,
    password: env.DB_PASSWORD,
    logging: false, //console.log
  },
  sessions: {
    secret: env.SESSION_SECRET,
    cookieName: 'etSession',
    storePath: env.SESSION_STORE_PATH || '/tmp/et-sessions',
    lifetime: 3600,
    saveUninitialized: false
  },
  ldap: {
    serverUrl: env.LDAP_SERVER_URL,
    dn: env.LDAP_DN || 'ad',
    searchBase: env.LDAP_SEARCH_BASE
  }
  
};
