module.exports = {
  db: {
    logging: false, //console.log
  },
  sessions: {
    saveUninitialized: true
  },
  ldapAuth: {
    reconnect: true,
    connectTimeout: 10 * 1000,
    timeout: 10 * 1000
  }
};
