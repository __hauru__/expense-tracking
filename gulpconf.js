exports.dirs = {
  'public': {
    source: 'dev/',
    output: 'live/',
    jsFiles: ['libs.js', 'libs-precompiled.js', 'app.js', 'login-form.js', 'report.js'],
    jsOptions: {
      'libs.js': { uglify: true },
      'libs-precompiled.js': { uglify: false }
    }
  },
};
